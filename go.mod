module gitlab.com/simonklb/ff-dmenu

go 1.16

require (
	github.com/mitchellh/go-homedir v1.1.0
	github.com/smartystreets/goconvey v1.6.4 // indirect
	gopkg.in/ini.v1 v1.62.0
)
