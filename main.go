package main

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"sort"
	"strings"
	"syscall"

	"github.com/mitchellh/go-homedir"
	"gopkg.in/ini.v1"
)

type parsingError struct {
	path string
	err  error
}

func (e *parsingError) Error() string {
	return fmt.Sprintf("error parsing profiles file (%s): %s", e.path, e.err)
}

func profilesPath() (string, error) {
	home, err := homedir.Dir()
	if err != nil {
		return "", err
	}

	return filepath.Join(home, ".mozilla/firefox/profiles.ini"), nil
}

func profileNames(profilesFilePath string) ([]string, error) {
	profiles, err := ini.Load(profilesFilePath)
	if err != nil {
		return nil, &parsingError{profilesFilePath, err}
	}

	var names []string

	for _, profile := range profiles.Sections() {
		if !strings.HasPrefix(profile.Name(), "Profile") {
			continue
		}

		k, err := profile.GetKey("Name")
		if err != nil {
			return nil, &parsingError{profilesFilePath, err}
		}

		names = append(names, k.Value())
	}

	sort.Strings(names)

	return names, nil
}

func dmenu(input []string) (string, error) {
	dmenu := exec.Command("dmenu", "-i")
	dmenu.Stdin = strings.NewReader(strings.Join(input, "\n"))

	output, err := dmenu.Output()
	if err != nil {
		var exitErr *exec.ExitError
		if errors.As(err, &exitErr) {
			fmt.Fprintln(os.Stderr, string(exitErr.Stderr))
		}
		return "", err
	}

	return strings.TrimSuffix(string(output), "\n"), nil
}

func run() (err error) {
	bin, err := exec.LookPath("firefox")
	if err != nil {
		return
	}

	p, err := profilesPath()
	if err != nil {
		return fmt.Errorf("error building profiles path: %w", err)
	}

	names, err := profileNames(p)
	if err != nil {
		return
	}

	profile, err := dmenu(names)
	if err != nil {
		return
	}

	return syscall.Exec(bin, []string{"firefox", "-P", profile}, os.Environ())
}

func main() {
	if err := run(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
