package main

import "testing"

func TestProfileNames(t *testing.T) {
	expected := []string{"Bar", "Foo"}

	names, err := profileNames("./testdata/profiles.ini")
	if err != nil {
		t.Fatal(err)
	}

	if len(names) != len(expected) {
		t.Fatal("wrong number of profile names")
	}

	for i := range names {
		if names[i] != expected[i] {
			t.Errorf("expected %s got %s", names[i], expected[i])
		}
	}
}
